package com.ubitricity.maksb;

import com.ubitricity.maksb.model.ChargingConnection;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertEquals;

public class ChargingServiceTest {

    private ChargingService sut;

    private ChargingConnection chargingConnection;

    @BeforeMethod
    public void setUp() {
        initMocks(this);
        chargingConnection = new ChargingConnection();
        chargingConnection.setChargingPointId("cpId");
    }

    @Test
    public void shouldReturn20_whenThereIsEnoughCapacity() {
        sut = new ChargingService(20);

        ChargingConnection actual = sut.createChargingConnection(chargingConnection);

        assertEquals(actual.getCurrent(), 20);
        assertEquals(actual.getMaxAvailable(), 20);
        assertEquals(sut.getAvailableCapacity(), 0);
    }

    @Test
    public void shouldReturn10_whenThereIsNotEnoughCapacity() {
        sut = new ChargingService(11);

        ChargingConnection actual = sut.createChargingConnection(chargingConnection);

        assertEquals(actual.getCurrent(), 10);
        assertEquals(actual.getMaxAvailable(), 10);
        assertEquals(sut.getAvailableCapacity(), 1);
    }

    @Test
    public void shouldReturn20_whenThereIsMoreThanEnoughCapacity() {
        sut = new ChargingService(100);

        ChargingConnection actual = sut.createChargingConnection(chargingConnection);

        assertEquals(actual.getCurrent(), 20);
        assertEquals(actual.getMaxAvailable(), 20);
        assertEquals(sut.getAvailableCapacity(), 80);
    }

    @Test
    public void shouldReturn0_whenNoCurrentCapacity() {
        sut = new ChargingService(5);

        ChargingConnection actual = sut.createChargingConnection(chargingConnection);

        assertEquals(actual.getCurrent(), 0);
        assertEquals(actual.getMaxAvailable(), 0);
        assertEquals(sut.getAvailableCapacity(), 5);
    }

    @Test(expectedExceptions = ChargingPointAlreadyUsed.class)
    public void shouldThrowException_whenChargingPointIsAlreadyUsed() {
        sut = new ChargingService(5);
        sut.createChargingConnection(chargingConnection);

        sut.createChargingConnection(chargingConnection);
    }

    @Test(expectedExceptions = ChargingNotFoundException.class)
    public void shouldThrowException_whenUpdateUnknownCharging() {
        sut = new ChargingService(5);

        sut.updateChargingConnection("unknownId", chargingConnection);
    }

    @Test
    public void shouldUpdateCurrent_whenUpdateToSmaller() {
        sut = new ChargingService(21);
        ChargingConnection created = sut.createChargingConnection(this.chargingConnection);
        ChargingConnection request = new ChargingConnection();
        request.setCurrent(10);

        ChargingConnection updated = sut.updateChargingConnection(created.getChargingPointId(), request);

        assertEquals(updated.getCurrent(), 10);
        assertEquals(updated.getMaxAvailable(), 20);
        assertEquals(updated.getChargingPointId(), chargingConnection.getChargingPointId());
        assertEquals(sut.getAvailableCapacity(), 11);
    }

    @Test
    public void shouldReturnCharging() {
        sut = new ChargingService(21);
        ChargingConnection created = sut.createChargingConnection(this.chargingConnection);

        ChargingConnection got = sut.getCharging(created.getChargingPointId());

        assertEquals(got, created);
    }

    @Test(expectedExceptions = ChargingNotFoundException.class)
    public void shouldThrowException_whenChargingNotFound() {
        sut = new ChargingService(21);

        sut.getCharging("someId");
    }

    @Test
    public void shouldReduceMaxCurrentForPrevious_and_return0ForNewConnection_whenAnotherCPIsConnected() {
        sut = new ChargingService(20);
        ChargingConnection first = sut.createChargingConnection(ChargingConnection.of("first"));

        ChargingConnection second = sut.createChargingConnection(ChargingConnection.of("second"));
        assertEquals(second.getCurrent(), 0);
        assertEquals(second.getMaxAvailable(), 0);
        ChargingConnection firstGot = sut.getCharging(first.getChargingPointId());
        assertEquals(firstGot.getCurrent(), 20);
        assertEquals(firstGot.getMaxAvailable(), 10);
    }

    @Test
    public void shouldReduceMaxCurrentForPreviousOnlyForOlderConnection_whenAnotherCPIsConnected() throws InterruptedException {
        sut = new ChargingService(50);
        ChargingConnection first = sut.createChargingConnection(ChargingConnection.of("first"));
        Thread.sleep(5);
        ChargingConnection second = sut.createChargingConnection(ChargingConnection.of("second"));

        ChargingConnection actual = sut.createChargingConnection(ChargingConnection.of("third"));

        assertEquals(actual.getCurrent(), 10);
        assertEquals(actual.getMaxAvailable(), 10);
        ChargingConnection firstGot = sut.getCharging(first.getChargingPointId());
        assertEquals(firstGot.getCurrent(), 20);
        assertEquals(firstGot.getMaxAvailable(), 10);
        ChargingConnection secondGot = sut.getCharging(second.getChargingPointId());
        assertEquals(secondGot.getCurrent(), 20);
        assertEquals(secondGot.getMaxAvailable(), 20);
    }

    @Test
    public void shouldAllowMaxCurrent_whenExistingRemoved() throws InterruptedException {
        sut = new ChargingService(20);
        ChargingConnection first = sut.createChargingConnection(ChargingConnection.of("first"));
        Thread.sleep(5);
        ChargingConnection second = sut.createChargingConnection(ChargingConnection.of("second"));

        sut.deleteConnection(first.getChargingPointId());

        ChargingConnection actual = sut.getCharging(second.getChargingPointId());

        assertEquals(actual.getCurrent(), 0);
        assertEquals(actual.getMaxAvailable(), 20);
        assertEquals(sut.getAvailableCapacity(), 10);
    }
}