package com.ubitricity.maksb;

import com.ubitricity.maksb.model.ChargingConnection;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.OffsetDateTime;
import java.time.temporal.ChronoUnit;

import static org.testng.Assert.assertEquals;

public class EmptyYoungConnectionEarlierComparatorTest {

    private EmptyYoungConnectionEarlierComparator sut;
    private ChargingConnection first;
    private ChargingConnection second;

    @BeforeMethod
    public void setUp() {
        sut = new EmptyYoungConnectionEarlierComparator();
        first = new ChargingConnection();
        second = new ChargingConnection();
    }

    @Test
    public void fullerShouldComeEarlier() {
        first.setCurrent(10);
        second.setCurrent(20);
        OffsetDateTime now = OffsetDateTime.now();
        first.setCreatedAt(now);
        second.setCreatedAt(now);

        assertEquals(sut.compare(first, second), -1);
    }


    @Test
    public void olderShouldComeEarlier() {
        first.setCurrent(10);
        second.setCurrent(10);
        OffsetDateTime now = OffsetDateTime.now();
        first.setCreatedAt(now);
        second.setCreatedAt(now.minus(10, ChronoUnit.MILLIS));

        assertEquals(sut.compare(first, second), -1);
    }
}