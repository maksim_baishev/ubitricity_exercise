package com.ubitricity.maksb;

import com.ubitricity.maksb.model.ChargingPoint;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertSame;

public class ChargingPointsServiceTest {

    private ChargingPointsService sut;
    @Mock
    private ChargingPointsRepository repository;

    @BeforeMethod
    public void setUp() throws Exception {
        initMocks(this);
        sut = new ChargingPointsService(repository);
    }

    @Test
    public void shouldReturnChargingPoints() throws Exception {
        List points = mock(List.class);
        when(repository.findAll()).thenReturn(points);

        List<ChargingPoint> actual = sut.getChargingPoints();

        assertSame(actual, points);
    }

    @Test
    public void shouldReturnChargingPoint() {
        ChargingPoint chargingPoint = new ChargingPoint();
        String cpId = "cpId";
        when(repository.findById(cpId)).thenReturn(Optional.of(chargingPoint));

        ChargingPoint actual = sut.getChargingPoint(cpId);

        assertSame(actual, chargingPoint);
    }

    @Test(expectedExceptions = ChargingPointNotFoundException.class)
    public void shouldThrowException_whenChargingPointUnknown() {
        String cpId = "cpId";
        when(repository.findById(cpId)).thenReturn(Optional.empty());

        sut.getChargingPoint(cpId);
    }
}
