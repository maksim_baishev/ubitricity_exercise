package com.ubitricity.maksb;

import com.ubitricity.maksb.model.ChargingPoint;
import com.ubitricity.maksb.model.ChargingPoints;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;

import java.util.List;

import static org.testng.Assert.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class IntegrationTest extends AbstractTestNGSpringContextTests {
    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void getChargingPointsInfo() {
        ResponseEntity<ChargingPoints> entity = restTemplate.getForEntity("/charging-points", ChargingPoints.class);
        assertEquals(entity.getStatusCode(), HttpStatus.OK);
        List<ChargingPoint> items = entity.getBody().getItems();
        assertEquals(items.size(), 10);
        System.out.println(items);
    }
}
