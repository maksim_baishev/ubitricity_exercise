package com.ubitricity.maksb;

import com.ubitricity.maksb.model.ChargingPoint;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class ChargingPointsService {

    private final ChargingPointsRepository repository;


    public List<ChargingPoint> getChargingPoints() {
        return repository.findAll();
    }


    public ChargingPoint getChargingPoint(String cpId) {
        return repository.findById(cpId).orElseThrow(ChargingPointNotFoundException::new);
    }
}
