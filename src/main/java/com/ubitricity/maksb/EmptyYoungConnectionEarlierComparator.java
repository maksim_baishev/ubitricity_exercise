package com.ubitricity.maksb;

import com.ubitricity.maksb.model.ChargingConnection;

import java.util.Comparator;

public class EmptyYoungConnectionEarlierComparator implements Comparator<ChargingConnection> {

    @Override
    public int compare(ChargingConnection c1, ChargingConnection c2) {
        // first should come connections with smaller current
        int cmp = Integer.compare(c1.getCurrent(), c2.getCurrent());
        if (cmp == 0) {
            // younger should come earlier
            cmp = c2.getCreatedAt().compareTo(c1.getCreatedAt());
        }
        return cmp;
    }
}
