package com.ubitricity.maksb;

import com.ubitricity.maksb.model.ChargingPoint;

import java.util.List;
import java.util.Optional;

public interface ChargingPointsRepository {

    List<ChargingPoint> findAll();

    Optional<ChargingPoint> findById(String cpId);

    ChargingPoint save(ChargingPoint chargingPoint);
}
