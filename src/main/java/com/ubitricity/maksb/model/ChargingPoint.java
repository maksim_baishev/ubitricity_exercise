package com.ubitricity.maksb.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.OffsetDateTime;

@Data
@EqualsAndHashCode
public class ChargingPoint {

    private String identifier;
    private int consumedCurrent;
    private OffsetDateTime lastUpdated;

    public static ChargingPoint copy(ChargingPoint chargingPoint) {
        ChargingPoint cp = new ChargingPoint();
        cp.setIdentifier(chargingPoint.getIdentifier());
        cp.setConsumedCurrent(chargingPoint.getConsumedCurrent());
        return cp;
    }
}
