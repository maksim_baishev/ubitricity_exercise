package com.ubitricity.maksb.model;

import lombok.Data;

@Data
public class CurrentConsumption {
    private int consumedCurrent;

    public static CurrentConsumption of(int current) {
        CurrentConsumption consumption = new CurrentConsumption();
        consumption.setConsumedCurrent(current);
        return consumption;
    }
}
