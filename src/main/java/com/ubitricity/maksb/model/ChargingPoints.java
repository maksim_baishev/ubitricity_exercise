package com.ubitricity.maksb.model;

import lombok.Data;

import java.util.List;

@Data
public class ChargingPoints {

    private List<ChargingPoint> items;

    public static ChargingPoints of(List<ChargingPoint> points) {
        ChargingPoints chargingPoints = new ChargingPoints();
        chargingPoints.setItems(points);
        return chargingPoints;
    }
}
