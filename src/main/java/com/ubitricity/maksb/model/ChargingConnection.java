package com.ubitricity.maksb.model;

import lombok.Data;

import java.time.OffsetDateTime;

@Data
public class ChargingConnection {
    private String chargingPointId;
    private int current;
    private int maxAvailable;
    private OffsetDateTime createdAt;

    public static ChargingConnection of(String chargingPointId) {
        ChargingConnection connection = new ChargingConnection();
        connection.setChargingPointId(chargingPointId);
        return connection;
    }
}
