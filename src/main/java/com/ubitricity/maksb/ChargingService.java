package com.ubitricity.maksb;

import com.ubitricity.maksb.model.ChargingConnection;
import lombok.Getter;
import lombok.Synchronized;
import org.springframework.stereotype.Component;

import java.time.OffsetDateTime;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class ChargingService {
    private static final int MAX_CURRENT = 20;
    private static final int NORMAL_CURRENT = 10;

    private final Object capacityLock = new Object();
    private final Object connectionsLock = new Object();

    private final int maxCapacity;
    @Getter
    private int availableCapacity;
    private Map<String, ChargingConnection> chargings = new ConcurrentHashMap<>();

    public ChargingService(int maxCapacity) {
        this.maxCapacity = maxCapacity;
        this.availableCapacity = maxCapacity;
    }

    @Synchronized("connectionsLock")
    public ChargingConnection createChargingConnection(ChargingConnection chargingConnection) {
        if (chargings.containsKey(chargingConnection.getChargingPointId())) {
            throw new ChargingPointAlreadyUsed();
        }
        chargingConnection.setCreatedAt(OffsetDateTime.now());
        chargings.put(chargingConnection.getChargingPointId(), chargingConnection);

        reserveMaxCapacity(chargingConnection);
        return chargingConnection;
    }

    @Synchronized("capacityLock")
    private void reserveMaxCapacity(ChargingConnection chargingConnection) {
        if (getAvailableCapacity() >= MAX_CURRENT) {
            chargingConnection.setCurrent(MAX_CURRENT);
        } else if (getAvailableCapacity() >= NORMAL_CURRENT) {
            chargingConnection.setCurrent(NORMAL_CURRENT);
        }
        chargingConnection.setMaxAvailable(chargingConnection.getCurrent());

        redistributeCurrent();
    }

    private void redistributeCurrent() {
        List<ChargingConnection> connections = new ArrayList<>(chargings.values());
        connections.sort(new EmptyYoungConnectionEarlierComparator());
        availableCapacity = maxCapacity;
        int requiredCapacity = 0;
        for (ChargingConnection connection : connections) {
            availableCapacity -= connection.getCurrent();
            requiredCapacity += MAX_CURRENT - connection.getCurrent();
        }
        Iterator<ChargingConnection> zeroCurrentIterator = connections.iterator();
        int availableLeftover = availableCapacity;
        while (availableLeftover >= NORMAL_CURRENT && zeroCurrentIterator.hasNext()) {
            ChargingConnection zeroCurrentConnection = zeroCurrentIterator.next();
            if (zeroCurrentConnection.getCurrent() == 0) {
                zeroCurrentConnection.setMaxAvailable(NORMAL_CURRENT);
                requiredCapacity -= NORMAL_CURRENT;
                availableLeftover -= NORMAL_CURRENT;
            } else {
                break;
            }
        }
        Iterator<ChargingConnection> normalCurrentIterator = connections.iterator();
        while (availableLeftover >= MAX_CURRENT && normalCurrentIterator.hasNext()) {
            ChargingConnection normalCurrentConnection = normalCurrentIterator.next();
            if (normalCurrentConnection.getCurrent() == NORMAL_CURRENT) {
                normalCurrentConnection.setMaxAvailable(MAX_CURRENT);
                requiredCapacity -= MAX_CURRENT - NORMAL_CURRENT;
                availableLeftover -= MAX_CURRENT - NORMAL_CURRENT;
            } else {
                break;
            }
        }
        for (int i = 0; i < connections.size(); ++i) {
            ChargingConnection oldConnection = connections.get(connections.size() - i - 1);
            if (oldConnection.getCurrent() == MAX_CURRENT && requiredCapacity > 0) {
                oldConnection.setMaxAvailable(NORMAL_CURRENT);
                requiredCapacity -= NORMAL_CURRENT;
            } else {
                break;
            }
        }
    }

    @Synchronized("capacityLock")
    public ChargingConnection updateChargingConnection(String id, ChargingConnection chargingConnection) {
        ChargingConnection existing = chargings.get(id);
        if (existing == null) {
            throw new ChargingNotFoundException();
        }
        if (chargingConnection.getCurrent() <= (getAvailableCapacity() + existing.getCurrent())) {
            String chargingPointId = existing.getChargingPointId();
            chargingConnection.setChargingPointId(chargingPointId);
            chargingConnection.setMaxAvailable(existing.getCurrent());
            chargings.put(chargingPointId, chargingConnection);
            redistributeCurrent();
            return chargingConnection;
        } else {
            throw new NotEnoughCapacityException();
        }
    }

    @Synchronized("capacityLock")
    private void internalDeleteConnection(String id) {
        ChargingConnection chargingConnection = chargings.get(id);
        if (chargingConnection != null) {
            chargings.remove(id);
            redistributeCurrent();
        }
    }

    public void deleteConnection(String id) {
        ChargingConnection chargingConnection = chargings.get(id);
        if (chargingConnection != null) {
            internalDeleteConnection(id);
        }
    }

    public ChargingConnection getCharging(String chargingPointId) {
        return Optional.ofNullable(chargings.get(chargingPointId))
                .orElseThrow(ChargingNotFoundException::new);
    }
}
