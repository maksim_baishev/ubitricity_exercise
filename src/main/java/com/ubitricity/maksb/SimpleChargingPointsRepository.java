package com.ubitricity.maksb;

import com.ubitricity.maksb.model.ChargingPoint;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Component
public class SimpleChargingPointsRepository implements ChargingPointsRepository {

    private final List<ChargingPoint> points;

    public SimpleChargingPointsRepository() {
        points = new ArrayList<>();
        for (int i = 0; i < 10; ++i) {
            ChargingPoint cp = new ChargingPoint();
            cp.setIdentifier("CP" + (i + 1));
            points.add(cp);
        }
    }

    @Override
    public List<ChargingPoint> findAll() {
        return Collections.unmodifiableList(points);
    }

    @Override
    public Optional<ChargingPoint> findById(String cpId) {
        return points.stream()
                .filter(cp -> cpId.equals(cp.getIdentifier()))
                .map(ChargingPoint::copy)
                .findFirst();
    }

    @Override
    public ChargingPoint save(ChargingPoint chargingPoint) {
        ChargingPoint target = findById(chargingPoint.getIdentifier())
                .orElseThrow(ChargingPointNotFoundException::new);
        target.setConsumedCurrent(chargingPoint.getConsumedCurrent());
        return target;
    }
}
