package com.ubitricity.maksb;

import com.ubitricity.maksb.model.ChargingConnection;
import com.ubitricity.maksb.model.ChargingPoints;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
public class CarParkUbiController {

    private final ChargingPointsService chargingPointsService;
    private final ChargingService chargingService;

    @GetMapping("/charging-points")
    public ChargingPoints getChargingPoints() {
        return ChargingPoints.of(chargingPointsService.getChargingPoints());
    }

    @PostMapping("/charging-connections")
    public ChargingConnection createConnection(@RequestBody ChargingConnection chargingConnection) {
        // charging might have some useful data about client who connected to the CP
        return chargingService.createChargingConnection(chargingConnection);
    }

    @PutMapping("/charging-connections/{id}")
    public ChargingConnection updateConnection(@PathVariable String id,
                                               @RequestBody ChargingConnection chargingConnection) {
        return chargingService.updateChargingConnection(id, chargingConnection);
    }

    @DeleteMapping("/charging-connections/{id}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public Void deleteConnection(@PathVariable String id) {
        chargingService.deleteConnection(id);
        return null;
    }
}
