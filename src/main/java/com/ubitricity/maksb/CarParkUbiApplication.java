package com.ubitricity.maksb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CarParkUbiApplication {

    public static void main(String[] args) {
        SpringApplication.run(CarParkUbiApplication.class, args);
    }
}
