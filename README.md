# Coding challenge
**Car park Ubi**

## Domain vocabulary:
EV - electric vehicle.
CP - charging point, an element in an infrastructure that supplies electric energy for the recharging of electric vehicles

## Problem details:
The task is to implement a simple application to manage the charging points installed at Car park Ubi.
Car park Ubi has 10 charging points installed. When a car is connected it consumes either 20 Amperes(fast charging) or 10 Amperes (slow charging). 
Car park Ubi installation has an overall current input of 100 Amperes so it can support fast charging for a maximum of 5 cars or slow charging for a maximum of 10 cars at one time.
A charge point sends notification to the application when car is plugged or unplugged. Also each charging point which has a car plugged to it is polling the
Application every 2 minutes to ask which current the charging  point is allowed to provide to a car - 10 or 20 Amperes. 
The application must distribute the available current of 100 Amperes in between the charging points so that when possible all cars use fast charging and when the current is not sufficient some cars are switched to slow charging. 
Cars which were connected earlier have lower priority than those which were connected later. The application must also provide a report with a current state of each charging point
returning a list of charging point, status (free or occupied) and - if occupied – the consumed current.

## Constraints:
The solution must be implemented as a Spring Boot application exposing a REST API.
We appreciate unit tests and an integration test for the happy path.

## Examples:

```
CP1 sends a notification that a car is plugged
Report: 
CP1 OCCUPIED 20A
CP2 AVAILABLE
...
CP10 AVAILABLE
```

```
CP1, CP2, CP3, CP4, CP5 and CP6 send notification that a car is plugged
Report:
CP1 OCCUPIED 10A
CP2 OCCUPIED 10A
CP3 OCCUPIED 20A
CP4 OCCUPIED 20A
CP5 OCCUPIED 20A
CP6 OCCUPIED 20A
CP7 AVAILABLE
...
CP10 AVAILABLE
```

## Deliverables:
Link to the repository with the implementation and the documentation on how to call the API (Swagger/Postman collection/text description).
Please add any details about your ideas and considerations to this README and add it to the repository.

## Implementation notes

* charging points must not exceed 100 capacity limit
* charging point decides when to poll, and when to request a capacity
* redistribution of capacity is done after each connection changes: CP new connection, CP connection update, CP disconnect
* CP can have only 1 active connection. In order to create new one it should delete previous one
## Scenario
1. CP registers a connection, if there is no active connection, response is always successful, but current is set from available capacity 
2. Each CP polls for it's maximum current
3. If returned maximum current is less than consumed one it have to reduce it
4. If returned maximum current is bigger than consumed one, it can increase it
## Problems
* current capacity reserving logic has to be thread safe in order to avoid >100A consumption
* redistribution logic became too complicated to be implemented in short time 
### Endpoints
* GET /charging-points - returns list of charging points info
[
  {
    "id": <CP id>,
    "occupied": <true| false>,
    "current": <0, 10 or 20>
  }
]
* POST /charging-points/{id}/chargings create charging when a car connects to the point
{
  // some useful payload
}
returns
{
  "id": <id>,
  "current": <current: 0, 10 or 20>
}

description: always creates successful charging, but current might be 0, as we need to be sure that there is an available current capacity.
CP can GET available current later with GET /charging-points/{id} and then update charging current

* PUT /charging-points/{id}/chargings/{charging_id}
{
  "current": <requested current>
}

* GET /charging-points/{id}
{
  "id": <CP id>,
  "available_currents": [
     <values of available currents>
  ]
}
